//
//  CodingUserInfoKeyExtension.swift
//  citiesSquare1
//
//  Created by Carlos Fernandez on 21/08/2018.
//  Copyright © 2018 Carlos Fernandez. All rights reserved.
//

import Foundation

public extension CodingUserInfoKey {
    static let managedObjectContext = CodingUserInfoKey(rawValue: "managedObjectContext")
}
