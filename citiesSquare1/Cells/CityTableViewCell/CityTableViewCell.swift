//
//  CityTableViewCell.swift
//  citiesSquare1
//
//  Created by Carlos Fernandez on 22/08/2018.
//  Copyright © 2018 Carlos Fernandez. All rights reserved.
//

import UIKit

class CityTableViewCell: UITableViewCell {

    @IBOutlet weak var containNameLabel: UILabel!
    @IBOutlet weak var containLocalNameLabel: UILabel!
    @IBOutlet weak var countainCountryLabel: UILabel!
    @IBOutlet weak var countainLongLabel: UILabel!
    @IBOutlet weak var countainLatLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.selectionStyle = .none
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func configureCellWithCity(city:City) {
        self.containNameLabel.text = city.value(forKey: "name") as? String
        self.containLocalNameLabel.text = city.value(forKey: "localName") as? String
        self.countainCountryLabel.text = city.country.value(forKey: "name") as? String
        let longitude = city.value(forKey: "longitude") as? Double
        self.countainLongLabel.text = String(format:"%f", longitude!)
        let latitude = city.value(forKey: "latitude") as? Double
        self.countainLatLabel.text = String(format:"%f", latitude!)
    }
    
}
