//
//  ConnectToApiManager.swift
//  citiesSquare1
//
//  Created by Carlos Fernandez on 20/08/2018.
//  Copyright © 2018 Carlos Fernandez. All rights reserved.
//

import Foundation

protocol connectToApiProtocol {
    func cancelAllTasks()
}

extension connectToApiProtocol {
    func cancelAllTasks() {
        URLSession.shared.getTasksWithCompletionHandler { (urlSessionDataTaskArray, urlSessionUploadTaskArray, urlSessionDownloadTaskArray) in
            for dataTask in urlSessionDataTaskArray {
                dataTask.cancel()
            }
        }
    }
}

struct ConnecToApiManager {
    func connecToAPIWithURL(url:URL, method:String, completionHandler : @escaping (Data?,URLResponse?,Error?) -> Void){
        let urlSession = URLSession.shared
        let urlRequest = URLRequestManager().configureRequest(url: url, method: method)
        let datatask = SessionDataTaskManager().sessionDataTaskRequest(session: urlSession, request: urlRequest) { (data, urlResponse, error) in
            completionHandler(data,urlResponse,error)
        }
        datatask.resume()
    }
}
