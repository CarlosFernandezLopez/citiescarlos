//
//  URLRequestManager.swift
//  citiesSquare1
//
//  Created by Carlos Fernandez on 20/08/2018.
//  Copyright © 2018 Carlos Fernandez. All rights reserved.
//

import Foundation

struct URLRequestManager {
    
  func configureRequest (url:URL, method:String)->URLRequest {
        let urlRequest = URLRequest.init(url: url, cachePolicy:  NSURLRequest.CachePolicy.useProtocolCachePolicy, timeoutInterval: 10)
        return urlRequest
    }
}
