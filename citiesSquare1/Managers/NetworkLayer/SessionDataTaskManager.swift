//
//  SessionDataTaskManager.swift
//  citiesSquare1
//
//  Created by Carlos Fernandez on 20/08/2018.
//  Copyright © 2018 Carlos Fernandez. All rights reserved.
//

import Foundation

struct SessionDataTaskManager  {

    func sessionDataTaskRequest (session:URLSession, request:URLRequest, completionHandler : @escaping (Data?,URLResponse?,Error?) -> Void) ->URLSessionDataTask {
        let sessionDataTask  = session.dataTask(with: request) { (data, response, error) in
            guard error == nil else {
                // error
                return
            }
            
            guard let data = data else {
                // error
                return
            }
            completionHandler(data,response,error)
        }
        return sessionDataTask
    }
    
}
