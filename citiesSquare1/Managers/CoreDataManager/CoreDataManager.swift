//
//  CoreDataManager.swift
//  citiesSquare1
//
//  Created by Carlos Fernandez on 21/08/2018.
//  Copyright © 2018 Carlos Fernandez. All rights reserved.
//

import UIKit
import CoreData

class CoreDataManager: NSObject {
    
    static let sharedInstance = CoreDataManager()
    private override init() {}
    
    lazy var persistentContainer: NSPersistentContainer = {
        /*
         The persistent container for the application. This implementation
         creates and returns a container, having loaded the store for the
         application to it. This property is optional since there are legitimate
         error conditions that could cause the creation of the store to fail.
         */
        let container = NSPersistentContainer(name: "citiesSquare1")
        container.loadPersistentStores(completionHandler: { (storeDescription, error) in
            if let error = error as NSError? {
                // Replace this implementation with code to handle the error appropriately.
                // fatalError() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
                
                /*
                 Typical reasons for an error here include:
                 * The parent directory does not exist, cannot be created, or disallows writing.
                 * The persistent store is not accessible, due to permissions or data protection when the device is locked.
                 * The device is out of space.
                 * The store could not be migrated to the current model version.
                 Check the error message to determine what the actual problem was.
                 */
                fatalError("Unresolved error \(error), \(error.userInfo)")
            }
        })
        return container
    }()
    
    // MARK: - Core Data Saving support
    
    func saveContext () {
        let context = persistentContainer.viewContext
        if context.hasChanges {
            do {
                try context.save()
            } catch {
                // Replace this implementation with code to handle the error appropriately.
                // fatalError() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
                let nserror = error as NSError
                fatalError("Unresolved error \(nserror), \(nserror.userInfo)")
            }
        }
    }
    
    // MARK: - Fetching for stored cities
    
    func fetchCitiesFromCachedElements() -> [City]? {
        let managedObjectContext = persistentContainer.viewContext
        let fetchRequest = NSFetchRequest<City>(entityName: "City")
        do {
            let cities = try managedObjectContext.fetch(fetchRequest)
            return cities
        } catch let error {
            print(error)
            return nil
        }
    }
    
    // MARK: - Fetching for pagination
    
    func fetchStoredPagination() -> [Pagination]? {
        let managedObjectContext = persistentContainer.viewContext
        let fetchRequest = NSFetchRequest<Pagination>(entityName: "Pagination")
        do {
            let page = try managedObjectContext.fetch(fetchRequest)
            // update pagination if necessary
//            if page.count > 1 {
//               let pageNumberCero = page[0]
//               let pageNumberOne = page[1]
//                if pageNumberOne.value(forKey: "currentPage") as! Int > pageNumberCero.value(forKey: "currentPage") as! Int{
//                    do {
//                         managedObjectContext.delete(pageNumberCero)
//                        try managedObjectContext.save()
//                    } catch {
//                        print ("error updating pagination")
//                    }
//                }
//            }
            return page
        } catch let error {
            print(error)
            return nil
        }
    }
    
    func clearStorageForEntity(entity: String) {
        let managedObjectContext = persistentContainer.viewContext
        let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: entity)
        let batchDeleteRequest = NSBatchDeleteRequest(fetchRequest: fetchRequest)
        do {
            try managedObjectContext.execute(batchDeleteRequest)
        } catch let error as NSError {
            print(error)
        }
    }

}
