//
//  citiesViewModel.swift
//  citiesSquare1
//
//  Created by Carlos Fernandez on 20/08/2018.
//  Copyright © 2018 Carlos Fernandez. All rights reserved.
//

import Foundation
import UIKit

protocol citiesProtocol: class {
    var citySearched : String {get}
    var currentPage  : Int {get set}
    
    func fetchFromStorageArrayOfCities (completionHandler : @escaping ([City]) -> Void)
    func getCitiesFromApiURL(apiURL: URL, completionHandler: @escaping ([City]) -> Void)
}

extension citiesProtocol {
    
    func returnedApiURL(filter: String, pageNumber: Int) -> URL {
        let filteredFilter = filter.replacingOccurrences(of: "\n", with: "")
        let filteredAPIURL = URL(string: "http://connect-demo.mobile1.io/square1/connect/v1/city?filter[0][name][contains]=\(filteredFilter)&page=\(pageNumber)&include=country")
        let apiURL = URL(string: "http://connect-demo.mobile1.io/square1/connect/v1/city?page=\(pageNumber)&include=country")
        if filter.count > 0 {
            return filteredAPIURL!
        } else {
            return apiURL!
        }
    }
    
    func fetchFromStorageArrayOfCities (completionHandler : @escaping ([City]) -> Void) {
        var citiesArray : [City]?
        citiesArray = CoreDataManager.sharedInstance.fetchCitiesFromCachedElements()
        
        if let numberOfCities = citiesArray?.count {
            if numberOfCities > 0 {
                completionHandler(citiesArray!)
            } else {
                let APIurl = returnedApiURL(filter: citySearched, pageNumber: getCurrentPage())
                getCitiesFromApiURL(apiURL: APIurl) { (citiesArray) in
                    completionHandler(citiesArray)
                }
            }
        }
    }
    
    func getCitiesFromApiURL(apiURL: URL, completionHandler: @escaping ([City]) -> Void) {
        var citiesArray : [City]?
        
        guard let codingUserInfoKeyManagedObjectContext = CodingUserInfoKey.managedObjectContext else {
            fatalError("Failed to retrieve managed object context")
        }
        
        ConnecToApiManager().connecToAPIWithURL(url: apiURL, method: "GET") { (data, response, error) in
            guard error == nil else {
                // error
                return
            }
            
            guard let data = data else {
                // error
                return
            }
            let managedObjectContext = CoreDataManager.sharedInstance.persistentContainer.viewContext
            let decoder = JSONDecoder()
            decoder.userInfo[codingUserInfoKeyManagedObjectContext] = managedObjectContext
            do {
                _ = try decoder.decode(Response.self, from: data)
                do {
                    try managedObjectContext.save()
                    citiesArray = CoreDataManager.sharedInstance.fetchCitiesFromCachedElements()
                    self.getCurrentPage()
                    //paginationArray = CoreDataManager.sharedInstance.fetchStoredPagination()
                    completionHandler (citiesArray!)
                    print ("context saved")
                } catch {
                    print ("error saving context")
                }
            } catch {
                print ("error decoding response")
            }
            
        }
    }

}

private extension citiesProtocol {
    func pageNumberFromPageEntity(pageArray:[Pagination])-> Int {
        let paging = pageArray.last
        let pageNumber = paging!.value(forKey: "currentPage") as! Int
        return pageNumber
    }
    
    func getCurrentPage()->Int {
        var pageNumber = 0
        var paginationArray : [Pagination]?
        paginationArray = CoreDataManager.sharedInstance.fetchStoredPagination()
        if let numberOfPaginations = paginationArray?.count {
            if numberOfPaginations > 0 {
                pageNumber = pageNumberFromPageEntity(pageArray: paginationArray!)
                pageNumber += 1
                currentPage = pageNumber
            }
        }
        return pageNumber
    }
}

class CitiesViewModel : citiesProtocol, connectToApiProtocol {
    var citySearched : String = ""
    var currentPage: Int = 1
    
    func updateCitiesListWithInfiniteScrolling(row:Int, numberOfCities:Int, completionHanlder : @escaping ([City]?) -> Void) {
        if row == numberOfCities - 7 {
            let APIurl = returnedApiURL(filter:citySearched, pageNumber: getCurrentPage())
            getCitiesFromApiURL(apiURL: APIurl) { (citiesArray) in
                completionHanlder(citiesArray)
            }
        }
    }
    
    func updateAPIAndStorageWithFilter(filter:String, completionHanlder : @escaping ([City]?) -> Void) {
        cancelAllTasks()
        CoreDataManager.sharedInstance.clearStorageForEntity(entity:"City")
        // Here I was dumping the pagination before time, therefore showing wrong results.
        // No it is possible to filter correctly using two letters
        if filter == "" {
            CoreDataManager.sharedInstance.clearStorageForEntity(entity:"Pagination")
        }
        let APIurl = returnedApiURL(filter: filter, pageNumber: getCurrentPage())
        getCitiesFromApiURL(apiURL: APIurl) { (citiesArray) in
            completionHanlder(citiesArray)
        }
    }
}
