//
//  citiesViewController.swift
//  citiesSquare1
//
//  Created by Carlos Fernandez on 20/08/2018.
//  Copyright © 2018 Carlos Fernandez. All rights reserved.
//

import UIKit

class CitiesViewController: UIViewController {

    let citiesViewModel = CitiesViewModel()
    var citiesList : [City] = []
    var selectedCity : City?
    
    @IBOutlet weak var citiesTableView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        definesPresentationContext = true
        searchBarSetUp()
        citiesTableView.register(UINib.init(nibName: "CityTableViewCell", bundle: nil), forCellReuseIdentifier: "cityTableViewCell")
        citiesViewModel.fetchFromStorageArrayOfCities { (citiesArray) in
                self.updateCityListWithArray(arrayOfCities: citiesArray)
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func searchBarSetUp() {
        navigationItem.title = "Cities"
        let searchController = UISearchController(searchResultsController: nil)
        navigationItem.searchController =  searchController
        navigationItem.hidesSearchBarWhenScrolling = false
        navigationItem.searchController?.searchBar.delegate = self
        navigationItem.searchController?.obscuresBackgroundDuringPresentation = false
    }
    // MARK: - Navigation

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        let mapViewController = segue.destination as! MapDetailUViewController
        mapViewController.city = self.selectedCity!
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }

}

extension CitiesViewController : UITableViewDataSource, UITableViewDelegate {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
       return self.citiesList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cityTableViewCell", for: indexPath) as! CityTableViewCell
        let rowCity = self.citiesList[indexPath.row]
        cell.configureCellWithCity(city:rowCity)
        if self.citiesList.count > 10 {
            citiesViewModel.updateCitiesListWithInfiniteScrolling(row: indexPath.row, numberOfCities: self.citiesList.count) { (citiesArray) in
                self.updateCityListWithArray(arrayOfCities: citiesArray!)
            }
        }
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        navigationItem.searchController?.searchBar.endEditing(true)
        self.selectedCity = self.citiesList[indexPath.row]
        performSegue(withIdentifier: "goToMapDetailFromCityList", sender: nil)
    }
    
    
}

extension CitiesViewController : UISearchBarDelegate {
    func searchBar(_ searchBar: UISearchBar, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        if let textSearch = searchBar.text,
            let textRange = Range(range, in: textSearch) {
            let updatedText = textSearch.replacingCharacters(in: textRange, with: text)
            citiesViewModel.citySearched = updatedText
            citiesViewModel.updateAPIAndStorageWithFilter(filter: citiesViewModel.citySearched) { (citiesArray) in
                self.updateCityListWithArray(arrayOfCities: citiesArray!)
            }
        }
        return true
    }
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        searchBar.endEditing(true)
        searchBar.text = ""
    }
    // Added cancel button. In the previous version pressing cancel button just dismissed de keyboard, but the consecutives searches would keep the filter string, therefore showing wrong results.
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        searchBar.endEditing(true)
        searchBar.text = ""
        citiesViewModel.citySearched = ""
        citiesViewModel.fetchFromStorageArrayOfCities { (citiesArray) in
            self.updateCityListWithArray(arrayOfCities: citiesArray)
        }
    }
}

private extension CitiesViewController {
    func updateCityListWithArray(arrayOfCities:[City]) {
        self.citiesList = arrayOfCities
        DispatchQueue.main.async {
            self.citiesTableView.reloadData()
            if self.citiesList.count == 0 {
                self.showError("No cities!", message: "Could not find any city")
            }
        }
        
    }
}



