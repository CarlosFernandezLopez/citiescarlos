//
//  MapDetailUViewController.swift
//  citiesSquare1
//
//  Created by Carlos Fernandez on 23/08/2018.
//  Copyright © 2018 Carlos Fernandez. All rights reserved.
//

import UIKit
import MapKit

class MapDetailUViewController: UIViewController {

    @IBOutlet weak var cityMap: MKMapView!
    var city : City!
    private let regionRadius: CLLocationDistance = 1000
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.setNavigationBarHidden(false, animated: false)
        titleName()
        configureAnnotationForCity(city: city!)
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    private func titleName (){
        let cityName = city.value(forKey: "name") as? String
        let countryName = city.country.value(forKey: "name") as? String
        self.title = "\(cityName ?? ""), \(countryName ?? "")"
    }
    private func configureAnnotationForCity(city:City) {
        let cityAnnotation = MKPointAnnotation()
        cityAnnotation.title = city.value(forKey: "name") as? String
        let lat = city.value(forKey: "latitude") as! Double
        let long = city.value(forKey: "longitude") as! Double
        cityAnnotation.coordinate = CLLocationCoordinate2D(latitude: lat, longitude: long)
        cityMap.addAnnotation(cityAnnotation)
        let cityLocation = CLLocation(latitude: lat, longitude: long)
        centerMapOnLocation(location: cityLocation)
        showErrorMessageIfNoCoordinates(latitude: lat, longitude: long)
    }
    
    private func showErrorMessageIfNoCoordinates(latitude:Double, longitude: Double) {
        if latitude.isEqual(to: 0.0) && longitude.isEqual(to: 0.0) {
            showError("No Coordinates", message: "No coordinates gotten for this city")
        }
    }
    
    
    private func centerMapOnLocation(location: CLLocation) {
        let coordinateRegion = MKCoordinateRegionMakeWithDistance(location.coordinate, regionRadius, regionRadius)
        cityMap.setRegion(coordinateRegion, animated: true)
    }
}

extension MapDetailUViewController: MKMapViewDelegate {
    func mapView(_ mapView: MKMapView, viewFor annotation: MKAnnotation) -> MKAnnotationView? {
        guard annotation is MKPointAnnotation else { return nil }
        
        let identifier = "cityAnnotation"
        var annotationView = mapView.dequeueReusableAnnotationView(withIdentifier: identifier)
        
        if annotationView == nil {
            annotationView = MKPinAnnotationView(annotation: annotation, reuseIdentifier: identifier)
            annotationView!.canShowCallout = true
        } else {
            annotationView!.annotation = annotation
        }
        
        return annotationView
    }
}
