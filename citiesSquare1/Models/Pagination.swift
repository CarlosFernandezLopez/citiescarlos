//
//  Pagination.swift
//  citiesSquare1
//
//  Created by Carlos Fernandez on 20/08/2018.
//  Copyright © 2018 Carlos Fernandez. All rights reserved.
//

import Foundation
import CoreData

class Pagination : NSManagedObject, Decodable {
    
    @NSManaged var currentPage : Int
    @NSManaged var lastPage : Int
    @NSManaged var perPage : Int
    @NSManaged var total : Int
    
    enum CodignKeys: String, CodingKey {
        case currentPage = "current_page"
        case lastPage = "last_page"
        case perPage = "per_page"
        case total
    }
    
    required convenience init(from decoder: Decoder) throws {
        
        guard let codingUserInfoKeyManagedObjectContext = CodingUserInfoKey.managedObjectContext,
            let managedObjectContext = decoder.userInfo[codingUserInfoKeyManagedObjectContext] as? NSManagedObjectContext,
            let entity = NSEntityDescription.entity(forEntityName: "Pagination", in: managedObjectContext) else {
                fatalError("Failed to decode pagination")
                
        }
        self.init(entity: entity, insertInto: managedObjectContext)
        
        let container = try decoder.container(keyedBy:CodignKeys.self)
        do {
            self.currentPage = try container.decode(Int.self, forKey: .currentPage)
        } catch {
            print ("error currentPage")
        }
        do {
            self.lastPage = try container.decode(Int.self,forKey: .lastPage)
        } catch  {
            print ("error lastPage")
        }
        do {
            self.perPage = try container.decode(Int.self,forKey: .perPage)
        } catch  {
            print ("error longitude")
        }
        do {
            self.total = try container.decode(Int.self,forKey: .total)
        } catch  {
            print ("error total")
        }
        
    }
    
}
