//
//  Country.swift
//  citiesSquare1
//
//  Created by Carlos Fernandez on 20/08/2018.
//  Copyright © 2018 Carlos Fernandez. All rights reserved.
//

import Foundation
import CoreData

class Country : NSManagedObject, Decodable {
    
    @NSManaged var identifier : Int
    @NSManaged var name : String?
    @NSManaged var code : String?
    @NSManaged var createdAt : String?
    @NSManaged var updatedAt : String?
    @NSManaged var continentId : Int
    

    private enum CodignKeys: String, CodingKey {
        case identifier = "id"
        case createdAt = "created_at"
        case updatedAt = "updated_at"
        case name
        case code
        case continentId = "continent_id"
    }
    
    required convenience init(from decoder: Decoder) throws {
        
        guard let codingUserInfoKeyManagedObjectContext = CodingUserInfoKey.managedObjectContext,
            let managedObjectContext = decoder.userInfo[codingUserInfoKeyManagedObjectContext] as? NSManagedObjectContext,
            let entity = NSEntityDescription.entity(forEntityName: "Country", in: managedObjectContext) else {
                fatalError("Failed to decode country")
                
        }
        self.init(entity: entity, insertInto: managedObjectContext)
        
        let container = try decoder.container(keyedBy:CodignKeys.self)
        do {
            self.identifier = try container.decode(Int.self, forKey: .identifier)
        } catch {
            print ("error country identifier")
        }
        do {
            self.name = try container.decode(String.self,forKey: .name)
        } catch {
            print ("error country name")
        }
        do {
            self.code = try container.decode(String.self, forKey: .code)
        } catch {
            print ("error country code")
        }
        do {
            self.createdAt = try container.decode(String.self,forKey: .createdAt)
        } catch {
            print ("error country created_at")
        }
        do {
            self.updatedAt = try container.decode(String.self,forKey: .updatedAt)
        } catch {
            print ("error country updated_at")
        }
        do {
            self.continentId = try container.decode(Int.self,forKey: .continentId)
        } catch  {
            print ("error continent ID")
        }
        
    }
}
