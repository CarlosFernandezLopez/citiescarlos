//
//  Response.swift
//  citiesSquare1
//
//  Created by Carlos Fernandez on 21/08/2018.
//  Copyright © 2018 Carlos Fernandez. All rights reserved.
//

import Foundation

struct Response : Decodable {
    
    let items : [City]
    let pagination : Pagination
    
    enum CodingKeys : String, CodingKey {
        case data = "data"
        case items = "items"
        case pagination
    }
    
    init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        let data = try container.nestedContainer(keyedBy:
            CodingKeys.self, forKey: .data)
        self.items = try data.decode([City].self, forKey: .items)
        self.pagination = try data.decode(Pagination.self, forKey: .pagination)
    }
    
}
