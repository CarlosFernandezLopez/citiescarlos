//
//  City.swift
//  citiesSquare1
//
//  Created by Carlos Fernandez on 20/08/2018.
//  Copyright © 2018 Carlos Fernandez. All rights reserved.
//

import Foundation
import CoreData

class City : NSManagedObject, Decodable {
    //
    @NSManaged var identifier : Int
    @NSManaged var name : String?
    @NSManaged var localName : String?
    @NSManaged var latitude :  Double
    @NSManaged var longitude : Double
    @NSManaged var createdAt : String?
    @NSManaged var updatedAt : String?
    @NSManaged var countryId : Int
    @NSManaged var country : Country
    
    private enum CodignKeys: String, CodingKey {
        case localName = "local_name"
        case createdAt = "created_at"
        case updatedAt = "updated_at"
        case identifier = "id"
        case latitude = "lat"
        case longitude = "lng"
        case name
        case countryId = "country_id"
        case country = "country"
    }
    
    required convenience init(from decoder: Decoder) throws {
        
        guard let codingUserInfoKeyManagedObjectContext = CodingUserInfoKey.managedObjectContext,
            let managedObjectContext = decoder.userInfo[codingUserInfoKeyManagedObjectContext] as? NSManagedObjectContext,
            let entity = NSEntityDescription.entity(forEntityName: "City", in: managedObjectContext) else {
                fatalError("Failed to decode city")
                
        }
        self.init(entity: entity, insertInto: managedObjectContext)
        
        do {
            let container = try decoder.container(keyedBy:CodignKeys.self)
            do {
                self.identifier = try container.decode(Int.self, forKey: .identifier)
            } catch {
                print ("error identifier")
            }
            do {
                self.name = try container.decode(String.self,forKey: .name)
            } catch {
                print ("error printing name")
            }
            do {
                self.localName = try container.decode(String.self,forKey: .localName)
            } catch {
                print ("error printing locale name")
            }
            do {
                self.createdAt = try container.decode(String.self,forKey: .createdAt)
            } catch {
                print ("error created at")
            }
            do {
                self.updatedAt = try container.decode(String.self,forKey: .updatedAt)
            } catch {
                print ("error updated at")
            }
            do {
                self.latitude = try container.decode(Double.self,forKey: .latitude)
            } catch  {
                print ("error latitude")
            }
            do {
                self.longitude = try container.decode(Double.self,forKey: .longitude)
            } catch  {
                print ("error longitude")
            }
            do {
                self.countryId = try container.decode(Int.self,forKey: .countryId)
                print (self.countryId)
            } catch  {
                print ("error country ID")
            }
            do {
                self.country = try container.decode(Country.self, forKey: .country)
            } catch {
                print ("error getting country")
            }
            
           
        } catch {
            print ("error getting city container")
        }
        
       
        
        
    }

}
